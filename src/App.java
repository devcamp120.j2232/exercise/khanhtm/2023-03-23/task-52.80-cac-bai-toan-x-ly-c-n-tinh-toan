public class App {
    public static void main(String[] args) throws Exception {
        //task 1
        double num = 2.130212;
        int n = 2;
        double roundedNum = Math.round(num * Math.pow(10, n)) / Math.pow(10, n);
        System.out.println("Ket qua sau khi lam tron: " + roundedNum);

        //task 2
        int a2 = 1;
        int b2 = 10;
        int randomNum = (int)(Math.random() * (b2 - a2 + 1)) + a2;
        System.out.println("So bat ky trong khoang cho truoc: " + randomNum);

        //task3
        double a3 = 3;
        double b3 = 4;
        double c3 = Math.sqrt(Math.pow(a3, 2) + Math.pow(b3, 2));
        System.out.println("Ket qua dinh ly pytago: " + c3);

        //task4
        int num4 = 16;
        int sqrt = (int) Math.sqrt(num4);
        if (sqrt * sqrt == num4) {
            System.out.println(num4 + " is a perfect square.");
        } else {
            System.out.println(num4 + " is not a perfect square.");
        }

        //task 5
        int num5 = 12;
        int result5 = (num5 + 4) / 5 * 5;
        System.out.println("So lon hon gan nhat chia het cho 5: " + result5);

        //task 6
        String str6 = "1234";
        try {
            double num6 = Double.parseDouble(str6);
            System.out.println(str6 + " is a valid number.");
        } catch (NumberFormatException e) {
            System.out.println(str6 + " is not a valid number.");
        }

        //task 7
        int num7 = 16;
        if ((num7 & (num7 - 1)) == 0 && num7 > 0) {
            System.out.println(num7 + " is a power of 2.");
        } else {
            System.out.println(num7 + " is not a power of 2.");
        }

        //task 8
        double num8 = 1.2;
        if (num8 == Math.floor(num8) && num8 >= 0) {
            System.out.println(num8 + " is a natural number.");
        } else {
            System.out.println(num8 + " is not a natural number.");
        }

        //task 9
        int num9 = 1234567;
        String formattedNum = String.format("%,d", num9);
        System.out.println(formattedNum);

        //task 10
        int num10 = 10;
        String binary = Integer.toBinaryString(num10);
        System.out.println("So chuyen sang ma nhi phan: " + binary);
    }


}
